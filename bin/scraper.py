#!/usr/bin/env python3

import argparse
import requests
from bs4 import BeautifulSoup
import os
from urllib.parse import urljoin
from datetime import datetime
from string import Template

def download_media(media_url, output_path):
    try:
        media_response = requests.get(media_url)
        media_response.raise_for_status()
        
        with open(output_path, 'wb') as file:
            file.write(media_response.content)
        print(f"Downloaded media and saved as: {output_path}")
    except requests.exceptions.RequestException as e:
        print(f"Failed to download media: {e}")

def load_template(template_path):
    """Load the external markdown template file."""
    with open(template_path, 'r') as template_file:
        return Template(template_file.read())

def scrape_content(url, project_folder, project_number, template):
    try:
        # Fetch the page content
        response = requests.get(url)
        response.raise_for_status()  # Raise an error for bad status codes
        soup = BeautifulSoup(response.text, 'html.parser')
        
        # Find the <h1 id="hero">
        hero_header = soup.find('h1', id='hero')
        
        if not hero_header:
            print("No <h1 id='hero'> found on the page.")
            return

        # Ensure the project folder exists
        os.makedirs(project_folder, exist_ok=True)
        
        # Search broadly for the first image or video after the <h1 id="hero">
        image_or_video_url = None
        file_name = None
        
        # Traverse siblings and children to find the first image or video
        current = hero_header
        while current:
            # Check for images inside paragraphs or directly after the header
            img = current.find('img')
            if img:
                image_or_video_url = urljoin(url, img['src'])
                file_ext = os.path.splitext(image_or_video_url)[1]  # Get file extension
                file_name = f"hero{file_ext}"
                download_media(image_or_video_url, os.path.join(project_folder, file_name))
                break
            
            # Check for videos inside paragraphs or directly after the header
            video = current.find('video')
            if video:
                source = video.find('source')
                if source:
                    image_or_video_url = urljoin(url, source['src'])
                    file_ext = os.path.splitext(image_or_video_url)[1]  # Get file extension
                    file_name = f"hero{file_ext}"
                    download_media(image_or_video_url, os.path.join(project_folder, file_name))
                    break

            # Move to the next sibling element
            current = current.find_next_sibling()

        if not image_or_video_url:
            print("No image or video found after <h1 id='hero'>.")
        
        # Find the first paragraph of text immediately after the image/video
        paragraph = None
        for sibling in hero_header.find_next_siblings():
            if sibling.name == 'p' and sibling.find('img') is None and sibling.find('video') is None:
                paragraph = sibling.get_text(strip=True)
                break
        
        if not paragraph:
            print("No paragraph found immediately after image/video.")
            paragraph = ""

        # Construct the markdown content using the external template
        title = f"Project {project_number:02}"
        today = datetime.now().strftime('%Y-%m-%d')
        img_path = f"/projects/{os.path.basename(project_folder)}/{file_name}" if file_name else ''
        image_section = f"![Project Image]({img_path})" if file_name else ''
        
        # Prepare the content to fill the template
        content = template.substitute(
            title=title,
            date=today,
            img=img_path,
            overview=paragraph,
            image_section=image_section
        )

        # Write the content to a markdown file
        output_markdown = os.path.join(project_folder, 'index.md')
        with open(output_markdown, 'w') as md_file:
            md_file.write(content)
        
        print(f"Content saved to {output_markdown}")
    
    except requests.exceptions.RequestException as e:
        print(f"Error fetching the URL: {e}")

def find_project_links(index_url):
    try:
        response = requests.get(index_url)
        response.raise_for_status()
        soup = BeautifulSoup(response.text, 'html.parser')

        # Find all project links
        project_links = []
        for a_tag in soup.find_all('a', href=True):
            href = a_tag['href']
            # Filter links based on common patterns in project URLs
            if 'projects' in href:
                full_url = urljoin(index_url, href)
                project_links.append(full_url)
        
        return project_links

    except requests.exceptions.RequestException as e:
        print(f"Error fetching the index URL: {e}")
        return []

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scrape hero content from project pages listed in an index page.')
    parser.add_argument('index_url', type=str, help='URL of the index page containing links to project pages')
    parser.add_argument('--template', type=str, default='project_template.md', help='Path to the markdown template file')
    parser.add_argument('--dest', type=str, default='.', help='Destination directory for project folders')
    args = parser.parse_args()

    # Load the external markdown template
    template = load_template(args.template)

    # Find all project links from the index page
    project_links = find_project_links(args.index_url)
    print(f"Found {len(project_links)} project links.")

    # Scrape each project page and save in its own folder in the destination directory
    for i, project_url in enumerate(project_links, 1):
        project_folder = os.path.join(args.dest, f"project_{i:02}")  # Zero-pad the folder names
        print(f"Scraping {project_url} and saving to {project_folder}...")
        scrape_content(project_url, project_folder, i, template)

