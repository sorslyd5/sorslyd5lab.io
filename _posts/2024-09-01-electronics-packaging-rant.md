---
layout: post
author: alan
id: the edc spectrum
aliases: []
tags: []
---

# meta

one of the big problems with the existing electronics packaging paradigm is how no one within each level seems to talk to the other levels.

## asep

ic packaging takes thermal design very seriously, but pcb designers don't necessarily do the same, nor does the infrastructure allow for good thermal design cheaply. ends up being a super interdisciplinary requirement to properly thermal design:

- level 2: "power" pcbs (4oz, not very thick)
- level 3: connectors need to support power
- active vs passive cooling

## lithography

the talent focused on lithography and building nano/micro devices don't really understand packaging and requirements there. separate domains of knowledge

# result

inefficiencies at each boundary because the goals are abstracted away from each other
- level 0 to level 1: bare die to chip packaging
    - this interface is actually pretty good
- level 1 to level 2: chip to board
    - thermal design suffers, not much data on thermal resistance, model, testing, environmental conditions, etc
    - active or passive cooling?
- level 2 to level 3: board to board
    - connectors bespoke (too many connector standards)
    - power capability of connectors is all over the place
        - no separation of concerns between application and capability, leading to a lot of specific configurations that cater to large market demand only
- level 3 to level 4: server stuff, networking
    - this is beyond the scope of this rant... for now

# aim

refactor electronics packaging from a discretely assembled perspective
do so while considering:

- existing supply chain
- faster better cheaper
- reconcile electronics, thermal, mechanical design
    - including design freedom enabled by technologies like 3d printing

not getting rid of electronics packaging hierarchy, but refactoring it to reduce inefficiencies, primarily at the interfaces

chiplets proved to be more scaleable thermally, economically, higher yield, which all resulted in higher performance
