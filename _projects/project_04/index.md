---
layout: post  # or a custom layout like project
title: Project 04
author: alan
date: 2024-11-27
img: /projects/project_04/hero.png
tags: [electronics, design]
---

# Project Overview

Modular mechanical keyboard with switches fabbed from the bottom up, discretely assembled with home made connectors!

# hero

![Project Image](/projects/project_04/hero.png)

