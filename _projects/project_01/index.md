---
layout: post  # or a custom layout like project
title: Project 01
author: alan
date: 2024-11-27
img: /projects/project_01/hero.jpg
tags: [electronics, design]
---

# Project Overview

the FabDeck is a portable soldering station for on-the-go hacking. Every year, I go to hackaday and bring my tools and projects in ziplock bags. No more! this year, I went with spiral 0 of my FabDeck, which received a lot of great feedback and vibes from the community.

# hero

![Project Image](/projects/project_01/hero.jpg)

