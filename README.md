 [![pipeline status](https://gitlab.com/sorslyd5/sorslyd5.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/sorslyd5/sorslyd5.gitlab.io/-/commits/master)

# meta

personal site

# how to dev locally

copied verbatim from Jake

to develop locally, pull the repo, install ruby, and install bundle using
`bundle install`

then run
`bundle exec jekyll serve`

