---
layout: home
title: home
nav_order: 1
mathjax: true
---

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=2.0">
    <style>
        .container {
            display: flex;
            justify-content: space-between;
        }
        .column {
            flex: 1;
            text-align: center;
            padding: 10px;
        }
        .column img {
            width: 90%; /* Adjust this value to control the image size */
            height: auto;
        }
        .caption {
            margin-top: 10px;
            font-size: 1rem; /* Adjust font size as needed */
        }
    </style>
    <!-- Import the component -->
    <script type="module" src="https://ajax.googleapis.com/ajax/libs/model-viewer/3.5.0/model-viewer.min.js"></script>
    <script async src="https://ga.jspm.io/npm:es-module-shims@1.7.1/dist/es-module-shims.js"></script>
    <script type="importmap">
    {
      "imports": {
        "three": "https://cdn.jsdelivr.net/npm/three@^0.167.1/build/three.module.min.js"
      }
    }
    </script>
    <script type="module" src="https://cdn.jsdelivr.net/npm/@google/model-viewer/dist/model-viewer-module.min.js"></script>
    <script type="module" src="https://cdn.jsdelivr.net/npm/@google/model-viewer-effects/dist/model-viewer-effects.min.js"></script>
</head>
<body>
    <div style="display: flex; justify-content: space-between;">
        <div style="flex: 1; padding-right: 10px;">
            <img src="{{ site.url }}{{ site.baseurl }}/fet.png" alt="head"/>
        </div>
        <div style="flex: 1; padding-left: 10px;">
            Hello there, my name is Alan! I'm a 2nd year Master's student in the Center for Bits and Atoms (CBA) in the MAS dept at MIT, and I'm exploring discrete electronics assembly.
        </div>
    </div>

    <div>
        <h1>Discrete electronics assembly at Scale</h1>
    </div>
    <div class="container">
        <div class="column">
            <a href="{{ site.baseurl }}/topics/1tiles">
                <img src="{{ site.url }}{{ site.baseurl }}/media/index/tiles.png" alt="tiles">
                <div class="caption">tiles</div>
            </a>
        </div>
        <div class="column">
            <a href="{{ site.baseurl }}/topics/2feeders">
                <img src="{{ site.url }}{{ site.baseurl }}/media/index/feeders.png" alt="feeders">
                <div class="caption">feeders</div>
            </a>
        </div>
        <div class="column">
            <a href="{{ site.baseurl }}/topics/3asm">
                <img src="{{ site.url }}{{ site.baseurl }}/media/index/assembly.gif" alt="assembly">
                <div class="caption">assembly</div>
            </a>
        </div>
        <!--
        <div class="column">
            <model-viewer alt="4Bp.glb" src="{{ site.url }}{{ site.baseurl }}/media/4Bp.glb" camera-controls touch-action="pan-y" skybox-height="1.5m" shadow-intensity="2">
                <effect-composer>
                    <outline-effect color="blue" blend-mode="skip">
                </effect-composer>
            </model-viewer>
            <div class="caption">4Bp</div>
        </div>
        -->
    </div>
    <div class="container">
        <div class="column">
            <a href="{{ site.baseurl }}/topics/4circuit">
                <img src="{{ site.url }}{{ site.baseurl }}/media/index/circuit.png" alt="circuit">
                <div class="caption">circuit</div>
            </a>
        </div>
        <div class="column">
            <a href="{{ site.baseurl }}/topics/5test">
                <img src="{{ site.url }}{{ site.baseurl }}/media/index/test.jpg" alt="test">
                <div class="caption">test</div>
            </a>
        </div>
    </div>
</body>

