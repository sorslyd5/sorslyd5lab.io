---
layout: default
title: circuit
nav_order: 5
mathjax: true
---

# meta

circuits are assembled in 3d

# design tools
## bottom-up

- kicad
- bottom-up 

# top-down

dice design tool

# circuits
## computational
- ring-osc
- nand
- adder

## power/actuation
- h-bridge

