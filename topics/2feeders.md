---
layout: default
title: feeders
nav_order: 3
mathjax: true
---

# meta

feeders hold the tiles for assembly

# design

- parametric, headless cadquery

# fabrication

- mSLA resin printed, flatness

