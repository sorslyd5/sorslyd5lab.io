---
layout: default
title: assembly
nav_order: 4
mathjax: true
---

# meta

cross between a pnp and 3d printer

assembly is currently a modified lumenpnp

dice asm v2 will be 3d specific

# design - ecad

- .pos files for pnp
- registration
- fixturing

# fabrication

batch feeders
alignment template
