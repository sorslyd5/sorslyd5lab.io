---
layout: post
title: geometry
parent: logic_10mm
grand_parent: research
---

# geometry
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# tiles and connectors

this geometry is based on a tiles and connectors paradigm, wherein connectors are used to selectively populate connections between tiles.
tiles can be either functional or routing.
abstraction level is at the individual component level, or transistor level in the case of the SERV processor being built.

## hierarchy

# tiles

tile system is based on the 4h geometry system.
- tiles
- macro-tiles

integrated power lines (single rail, typically 5v) greatly simplify and reduce routing tiles needed to complete circuits, but force certain valid rotation orientations due to limited axis' of symmetry
manually, there is an interesting workaround by flipping the tile, but in automatic builds, having enough variations of one tile is enough to consistently cover orientations.

![nets]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/nets.png)

## batch 001

| VCC                                                                 | GND                                                                 | I/O                                                                 |
| ------------------------------------------------------------------- | ------------------------------------------------------------------- | ------------------------------------------------------------------- |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132119.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132243.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132411.png) |
| 1206 (2-pin)                                                        | SOT-23 (3-pin)                                                      | cross (4-way)                                                       |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132352.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132229.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132309.png) |


| 8x8 macro-tile                                                      |                                                                      |
| ------------------------------------------------------------------- | -------------------------------------------------------------------- |
| ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/tiles/Screenshot_2024-03-27_132333.png) | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/circuits/PXL_20240327_164900041.cmp.jpg) |

{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b001" %}

## batch 002
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b002" %}

## batch 003
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b003" %}

## batch 004
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b004" %}

## batch 005
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b005" %}

## batch 007
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b007" %}

## batch 009
{% include image-gallery.html folder="/topics/research/media/remote/o_power/tiles/b009" %}

# connectors

## mcad - eye-of-the-needle connector (h_eon) automated pipeline

h-connector, eye-of-the-needle geometry, cadquery pipeline towards building CI/CD MCAD geometry

| 8-up carrier for h_eon geometry                                              |
| ---- |
| ![h_eon](media/remote/2024.04.02/Screenshot_2024-04-02_112728.cmp.png) |
 
- spent time refactoring h_eon cadquery code for better separation of concerns
- parametric sweeping and cloning
- tuned process to find good balance for reliable fablight fabrication

## h_eon v1.1

| ![](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/w09/media/remote/cad/h_eon_comparison.png)  |![](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/w09/media/remote/cad/tiebar.png) |
|---|---|
|  left, v1.0; right, v1.1 |tie-bar stub improvement |

the new geometry solves two major problems that improve reliability of assembled geometries:

- the eye-of-the-needle (eon) geometry was previously compromised and plastically deformed after repeated cycles because the oval was cut-off; the new geometry has a whole oval that is also above the joining strut, so the compliant pin behaves compliantly
- the tie-bar previously joined the body above the mating plane. on manual singulation, there would be a stub left over that could interfere with proper seating; the new version has an indent such that the stub left over is recessed


