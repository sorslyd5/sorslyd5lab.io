---
layout: post
title: logic_10mm
parent: research
---

# logic_10mm
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# background

- we're trying to build a discretely assembled computer out of electronic bricks
- a new way to breadboard
- COTS components are approaching early lithography feature sizes
- alternative to recycling e-waste

| discrete risc-v computer                                              |
| ---- |
| ![risc](media/remote/2024.04.02/Screenshot_2024-04-02_122637.cmp.png) |

## final video

[final video](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/final/final_mas865_web.mp4)

# dice & tools

| h_eon                                                               | singulated                                                          |
| ------------------------------------------------------------------- | ------------------------------------------------------------------- |
| ![](media/remote/2024.03.27/o_power/PXL_20240321_024227069.cmp.jpg) | ![](media/remote/2024.03.27/o_power/PXL_20240321_024401428.cmp.jpg) |
| single tile                                                         | test circuit                                                        |
| ![](media/remote/2024.03.27/o_power/PXL_20240321_024902096.cmp.jpg) | ![](media/remote/2024.03.27/o_power/PXL_20240321_041910811.cmp.jpg) |
| parts                                                               | lumenpnp positioning tray                                           |
| ![](media/remote/2024.03.27/o_power/PXL_20240321_042549184.cmp.jpg) | ![](media/remote/2024.03.27/o_power/PXL_20240321_043652306.cmp.jpg) |

- [end-effector updates](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/final/README.html#h-gripper-passive)
- [feeder updates](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/final/README.html#feeder-tray-and-dispenser)
- [hierarchy updates](https://fab.cba.mit.edu/classes/865.24/people/Alan/projects/final/README.html#error-correction-and-hierarchy)

# changelog
 
- 2024
    - Apr & May
        - batch 5 pcbs, connector v1.1
        - final machine demo, tile and h pnp
        - dispenser, end effector work
    - Mar
        - First batch of o_power arrived, first demonstration of functional circuits
        - Adapting Lumenpnp for DICE ops, start of machine building (danke)
        - work on end-effector tools (v9)
    - Feb
        - Lumenpnp helloworld
    - January
        - DICE geometry and end-effector tools
- 2023
    - December
        - experimenting with materials
    - November 
        - Visit from the Viteris vendor to bring our [topics/uEDM](topics/uEDM) up to speed. I got trained on the machine
        - Spent time getting familiar with CAM process and cutting brass with 50um wire
    - October
        - BvB project kickoff
