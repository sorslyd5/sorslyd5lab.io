---
layout: post
title: circuits
parent: dice
grand_parent: research
---

# circuits

| NMOS NAND gate                                                                       | diode AND gate                                                                              |
| ------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------- |
| ![asdf]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/circuits/PXL_20240327_164846612.cmp.jpg)                 | ![]({{ site.url }}{{ site.baseurl }}/topics/research/media/remote/2024.03.27/circuits/PXL_20240327_164852700.cmp.jpg)                        |
| ![](https://allabouteng.com/wp-content/uploads/2018/05/NMOS-NAND-Gate-Schematic.jpg) | ![](https://upload.wikimedia.org/wikipedia/commons/6/62/Animated_wired_AND_diode_logic.gif) |
