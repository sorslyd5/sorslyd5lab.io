---
layout: default
title: research
nav_order: 2
mathjax: true
has_children: true
---

# research
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# conventional electronics packaging
 
the conventional electronics packaging (EP) paradigm describes how electronics are physically packaged, from the bare die (transistors and moore's law), all the way to the end system (desktop vs laptop vs rackmount server farm).

![pkglvl](/media/pkging_lvls.png)

[image sourced from chegg](https://www.chegg.com/homework-help/questions-and-answers/purpose-printed-circuit-board-choose-apply-provides-radiation-protection-components-mounte-q89045686)

- level 0: bare die (transistors)
- level 1: IC package (DIP, SOT, BGA, etc.)
- level 2: substrate (PCB, FPC, MID, etc.)
- level 2.5: connectors (board to board, board to wire, edge mount, etc.)
    - note that interconnects have their own packaging levels, which would be x.5 between each electronics packaging level
- level 3: system (desktop vs laptop vs rackmount server farm)
- level 4: -
- level 5: -
- level 6: -

this EP structure describes physical (atoms) hierarchy whereas the OSI model describes digital (bits) hierarchy). in a sense, the higher levels of the electronics packaging levels (level 3 and beyond) represent where the OSI model starts to be relevant for communication and networking.

although less popular, interconnect (CKT) packaging levels are equally important and add resolution to the established electronics packaging level structure. as described by TE, they can be thought of as half-levels (x.5) between two adjacent electronics packaging levels.

![cktlvl](/media/ckt_lvls.png)

[interconnect packaging levels as defined by TE](https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1-1773853-5_levelspkg&DocType=DS&DocLang=EN)

although the established EP hierarchy has enabled and sustained the previous 2 digital revolutions (including moore's law, primarily ep level 0), multiple challenges have arisen with greater compute, power, thermal, efficiency demands.

# moore's law is dead, long live moore's law!

this [electronics packaging rant](/2024/09/01/electronics-packaging-rant.html) describes some of the friction at various interfaces (EP levels 1-3), with expanding scope (EP levels 0-4) after successful experiments.

# discretely assembled electronics - refactoring electronics packaging

my research ultimately aims to establish a new discretely assembled electronics packaging paradigm that reduces barriers between each level such that goals at the system level are easier to achieve (maximizing compute, thermal, efficiency, reconfigurability metrics).

on the road to reaching this new discrete electronics packaging system, the first challenge is to reduce frictions already inherent at the interface between existing electronics packaging levels.

## aim

refactor electronics packaging from a discretely assembled perspective
do so while considering:

- existing supply chain
- faster better cheaper
- reconcile electronics, thermal, mechanical design
    - taking advantage of design freedom enabled by technologies like 3d printing, which has been slowly moving from low-volume prototyping, to mid-scale, to high-volume production in some spaces

not getting rid of electronics packaging hierarchy, but refactoring it to reduce inefficiencies, primarily at the interfaces

chiplets proved to be more scaleable thermally, economically, higher yield, which all resulted in higher performance

## 4Lx

4Lx is an evolution of previous 4xx geometries that respects existing EP hierarchy.
 
- 4L1 - ic packaging, variable pin vmd proposal
    - non-selective interconnects(?)
    - edge to edge routing, n-pin
    - smt
    - 1 primitive class
- 4L2 - 3d substrate pcb replacement, compatible with 4L1
    - non-selective interconnects(?)
    - tile to tile routing(?)
    - smt
    - 1 primitive class
    - compared to 4B, 4Bc, much higher surface fill density
- 4L3 - board-to-board replacement (block-to-block), compatible with 4L2
    - wip
    
## trajectories

a lot of geometry development and exploration has taken place to "find" this new discrete EP hierarchy. generally, the following trends are observed:

- through hole -> surface mount
- reducing primitive classes to simplify end effector and assembly requirements
- maximizing design flexibility at the system level
- minimizing design flexibility at each individual EP level (1 EP level -> 1 job)
- flat geometries are more inherently compatible with established fab technologies and supply chain (flat components)

## why 4-sided geometry?

although some 3-sided and n-sided geometries have been explored, 4-sided geometries (4xx naming convention) have dominated.

- compatibility with existing cad programs
- cartesian grids are simpler
- compromise between routing flexbility (4 directions xy in-plane and 2 z out-plane) and existing component fanouts

## geometries

- geometry classification by connection (ckt) density
    - high density
        - 4L1 - ic packaging, variable pin vmd proposal
            - non-selective interconnects(?)
            - edge to edge routing, n-pin
            - smt
            - 1 primitive class
    - med density
        - 4Hp (o_power) - original h-based through-hole high density, keeps top-down synthesis efficient
            - selective interconnects
            - edge to edge routing, 4-pin
            - th
            - 2 primitive classes
        - 4Bp - like 4Hp but only tiles
            - selective interconnects
            - edge to edge routing, 4-pin
            - smt
            - 1 primitive class
    - low density
        - 4B - minimalist, Neil's geometry
            - non-selective interconnects(?)
            - tile to tile routing(?)
            - smt
            - 1 primitive class
        - 4Bc (vmd) - chamfered, self-aligning, mine
            - non-selective interconnects(?)
            - tile to tile routing(?)
            - smt
            - 1 primitive class
        - 4L2 - 3d substrate pcb replacement, compatible with 4L1
            - non-selective interconnects(?)
            - tile to tile routing(?)
            - smt
            - 1 primitive class
            - compared to 4B, 4Bc, much higher surface fill density

# benchmarking

## discrete display

## compute circuits

## discrete machine

## RISC-V SERV CPU

a further out benchmark is as follows.

we're trying to build a discretely assembled computer out of electronic lego bricks.
discrete electronics project, aiming to assemble a 10,000-part computer, discretely and automatically.
 
- a new way to breadboard
- COTS components are approaching early lithography feature sizes
- alternative to recycling e-waste
- reconfigurable electronics in the field
- adding rapid-prototyping of complex electronics packaging to the desktop fab repertoire

