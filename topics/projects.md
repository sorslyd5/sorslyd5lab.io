---
layout: default
title: Projects
nav_order: 3  # Adjust navigation order as needed
---

<h1>Latest Projects</h1>

<ul>
{% for project in site.projects %}
    <article class="project">
      {% if project.img %}
        <a class="project-thumbnail" style="background-image: url({{"/assets/img/" | prepend: site.baseurl | append: project.img}})" href="{{project.url | prepend: site.baseurl}}"></a>
      {% endif %}
        <h2 class="project-title"><a href="{{project.url | prepend: site.baseurl}}">{{project.title}}</a></h2>
        <p>{{ project.content | strip_html | truncatewords: 15 }}</p>
        <span class="project-date">{{project.date | date: '%Y, %b %d'}}&nbsp;&nbsp;&nbsp;—&nbsp;</span>
        <span class="project-words">{% capture words %}{{ project.content | number_of_words }}{% endcapture %}{% unless words contains "-" %}{{ words | plus: 250 | divided_by: 250 | append: " minute read" }}{% endunless %}</span>
    </article>
{% endfor %}
</ul>

