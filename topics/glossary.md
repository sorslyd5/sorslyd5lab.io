---
layout: default
title: glossary
nav_order: 9
mathjax: true
---

# meta

discrete assembly has a lot of terminology and adjacent spaces (machine building, fabrication, etc.)

# body

- volumetric mount device (VMD): the most basic building block used in discrete electronics assembly
- tile: synonymous with VMDs, specifically referring to geometries that are more 2d in construction
- end effector: usually refers to the vaccuum pick up nozzle or gripper claw for manipulating VMDs
- geometry family: refers to the specific construction of VMDs and toplogy strategy for constructing circuits using the VMDs
