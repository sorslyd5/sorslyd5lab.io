---
layout: default
title: test
nav_order: 6
mathjax: true
---

# meta

performance evaluations of assembled circuits

# device-to-device
- power
- parasitics
- discrete vs integrated

# circuits
- ring-osc performance
- thermals
- etc.

# housings
- shock/vibe
- environmental
- halt - highly accelerated life test
