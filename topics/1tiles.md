---
layout: default
title: tiles
nav_order: 2
mathjax: true
---

# meta

tiles make up the functional and routing blocks of circuit assemblies

this page covers: 
- tile types
- standard dimensions
- assembly topologies

# 4BI tiles

| ![./2a.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/2a.png) | ![./2b.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/2b.png) | ![./3a.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/3a.png) | ![./4b.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/4b.png) |
| 2a                              | 2b                              | 3a                              | 4b                              |
| ![./1206.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/1206.png) | ![./sot23.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/sot23.png) | ![./via_t.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/via_t.png) | ![./via_b.png]({{ site.url }}{{ site.baseurl }}/media/4BI/remote/via_b.png) |
| 1206                            | sot23                           | via, top                         | via, bottom                           |

# 4BI dimensions

because this geometry is tiled linearly at a 45 deg angle, the rectilinear pitch of this geometry is considered sqrt(actual pitch) * 2.
for compact batch feeding, tiles are typically stored at 0 deg angles, requiring a 45 degree reorient before placement.

- tile size: 5x5mm (4.9x4.9mm nom)
- actual pitch: 8mm
- lattice pitch: sqrt(actual pitch) * 2 = ~5.66mm

# 4BI assembly topologies

each layer in a 4BI assembly is technically 0.5 of a single circuit layer; at least 2 layers are required for a functioning circuit.

- 2-layer

---

# design - ecad

- kicad
- headless api-driven ecad is most parametric and easy

# fabrication

- milling
- pcbway

